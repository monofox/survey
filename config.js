import Icon from './img/survey.svg';

export default {
	id: "survey",
	meta: {
		title: 	{
			"de": "Online-Umfrage",
			"en": "Online Survey",
		},
		text:	{
			"de": "Durchführen von Umfragen; Fragebögen können selbst erstellt werden",
			"en": "Conducting surveys; questionnaires may be created by oneself"
		},
		to: 	"survey-index",
		adminto:"survey-admin",
		icon: 	Icon,
		index:	true,
		role: 	"survey",
		lock:	false
	},
	routes: [
		{	path: '/survey-index', name:'survey-index', component: () => import('./views/Index.vue') },
		{	path: '/survey-result', name:'survey-result', component: () => import('./views/Result.vue') },
		{	path: '/survey-tickets', name:'survey-tickets', component: () => import('./views/Tickets.vue') },
		{	path: '/survey', name:'survey-part', component: () => import('./views/Part.vue') },
		{	path: '/survey/:ticket', name:'survey-part-ticket', component: () => import('./views/Part.vue') },
		{	path: '/survey-preview', name:'survey-preview', component: () => import('./views/Preview.vue') },
		{	path: '/survey-edit', name:'survey-edit', component: () => import('./views/Edit.vue') },
		{	path: '/survey-admin', name:'survey-admin', component: () => import('./views/Admin.vue') },
	]
}
