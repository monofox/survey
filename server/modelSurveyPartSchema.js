var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var SurveyQuestionSchema=new Schema({
	text : {type: String, required: true, max: 100},
	type:	{type: Number, required: true},					// 1: yes-no, 2: agree, 3: text, 4: select
	options: {type: String, default:'', required:false}		// comma separated list, only for type 4
});

var SurveyPartSchema=new Schema({
	title: {type: String, required: true, max: 100},
	questions: [SurveyQuestionSchema]
});

// export the schema (which is then used in survey.js and surveyInstance.js)
module.exports=SurveyPartSchema;
