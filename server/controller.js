//External libs
const { check,param,body,validationResult }=require('express-validator/check');
const { sanitize }=require('express-validator/filter');
const generator=require('../../../main/server/passwordHelper');
const debug=require('debug')('survey');

//Internal libs
const config=__config;
const consts=require ('../../../consts.js');
const mainController=require('../../../main/server/controller');

//Database schemas
var Survey=require('./modelSurvey');
var SurveyInstance=require('./modelSurveyInstance');
var SurveyAnswer=require('./modelSurveyAnswer');

const TICKET_LENGTH=8;

/********************************************************************************
 Common functions
 ********************************************************************************/

 /*
  Check permission to use the tool
  */
 exports.checkAuth=function(req, res, next) {
 	mainController.checkAuthAndRole(req,res,next,"survey");
 }



/********************************************************************************
 Functions for author
 ********************************************************************************/

exports.getInstances=function(req, res, next) {
	SurveyInstance.find({owner: res.locals.user.login}, function (err, list) {
		if (err) debug(err);
		return res.json(list);
	});
}

exports.getSurveys=function(req, res, next) {
	Survey.find({ $or: [{owner: res.locals.user.login},{visible:true}]}, function(err,list) {
		if (err) debug(err);
		return res.json(list);
	})
}


/*
 Show results
 - Authentication via checkAuth (route)
 - Authorization with db and session.login
 */
exports.getResults=[
	sanitize('id').trim().escape(),
	(req, res, next)=> {
		var id=req.params.id;
		//Cancel user is not owner of instance
		SurveyInstance.findOne( {_id : id, owner: res.locals.user.login} ).exec(function(err,instance) {
			if (err) debug (err);
			if (!instance) return res.json([{msg:'forbidden'}]);
			//Aggregate answers
			SurveyAnswer.find( { instance : instance.id }).exec(function(err, answers) {
				if (err) debug(err);
				var numAnswers=answers.length;
				var results={};
				var idx=0;
				for (i=0; i<instance.parts.length;i++) {
					var part=instance.parts[i];
					for (j=0; j<part.questions.length; j++) {
						var question=part.questions[j];
						results[question._id]=[];
						if (question.type==1) results[question._id]=[0,0,0];
						if (question.type==2) results[question._id]=[0,0,0,0,0];
						if (question.type==4) {
							results[question._id]=[];
							var num=question.options.split(',').length;
							for (var k=0; k<=num; k++) results[question._id][k]=0;
						}
						for (k=0; k<answers.length; k++) {
							var answer=answers[k];
							if (question.type==1 || question.type==2 || question.type==4) results[question._id][answer.answers[idx]]++;
							if (question.type==3 && answer.answers[idx].length>0) results[question._id].push(answer.answers[idx]);
						}
						idx++;
					}
				}
				return res.json({instance:instance, numAnswers:numAnswers, results:results});
			});
		})
	}
]

/*
 Delete results
 - Authentication via checkAuth (route)
 - Authorization with db and session.login
 */
exports.delInstance=[
	param('id').isLength({ min: 1,max: 30 }).withMessage('validation-error'),
	sanitize('id').trim().escape(),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		var id=req.params.id;
		//Cancel, if user is not owner of instance (or admin is deleting)
		SurveyInstance.findById(id, function(err,instance) {
      	if (err) debug(err);
			if (!instance || (instance.owner!=res.locals.user.login && res.locals.user.roles.indexOf("admin")<0)) {
				return res.json([{msg:'forbidden'}]);
			}
			//Delete votes
			SurveyAnswer.deleteMany( { instance : id }, function(err, instance) {
				if (err) { debug(err); res.json([{msg:'database-error'}])}
      			//Delete instance itself
				SurveyInstance.deleteOne( { _id: id}, function(err, instance) {
					if (err) { debug(err); res.json([{msg:'database-error'}])}
					return res.json({});
				});
			});
		})
	}
]

/*
 Start new survey
 - Authentication via checkAuth (route)
 - Authorization with db and session.login and visibility of survey
 */
 exports.createInstance=[
	body('title').matches(consts.REGEX_TEXT_300),
	body('count').isInt({ min: 1, max: 1000 }),
	body('surveyId').isAlphanumeric(),
	sanitize('surveyId').trim(),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		//Cancel if user may not start survey
		var surveyId=req.body.surveyId;
		Survey.findById(surveyId, function(err,survey) {
			if (err || !survey) return res.json([{msg:'internal-error'}]);
			if (survey.owner!=res.locals.user.login && survey.visible==false) return res.json([{msg:'forbidden'}]);
			//Set survey number
			SurveyInstance.find({}).sort('-number').exec(function (err, member) {
				var number=1;
				if (member[0]) number=member[0].number+1;
				//Create tickets
				var i=0;
				var tickets=[];
				while (i<req.body.count) {
					var ticket=generator.generate({length: TICKET_LENGTH, numbers: true});
					if (tickets.indexOf(ticket)<0) {
						tickets[i]=number+"-"+ticket;
						i++;
					}
				}
				//Save to db
				instance=new SurveyInstance({
					title: req.body.title,
					surveyName: survey.name,
					number: number,
					owner: res.locals.user.login,
					tickets: tickets,
					invalidateTickets: tickets.length>1,
					parts: survey.parts,
					answers: [],
					created: new Date()
				});
				instance.save(function(err) {
					if(err) {debug(err); return res.json([{msg:'database-error'}]);}
					return res.json(instance);
				});
			});
		})
	}
]

/*
 Delete template
 - Authentication via checkAuth (route)
 - Authorization with loadSurvey (route)
 */
exports.deleteSurvey=[
	sanitize('id').trim().escape(),
	(req,res,next)=> {
		var id=req.params.id;
		var login=res.locals.user.login;
		var query;
		if (res.locals.user.roles.indexOf("admin")>=0) query={_id:id};	//admin
		else query={_id:id, owner:login}; //normal user
		Survey.deleteOne(query, function(err) {
			if (err) {
				debug(err);
				return res.json([{msg:'database-error'}]);
			}
			res.json({});
		})
	}
]

/*
 Save survey
 */
exports.saveSurvey=[
	sanitize('id').trim().escape(),
	(req,res,next)=> {
		var survey=parseJson(req.body.survey);
		survey.owner=res.locals.user.login;
		var id=req.params.id;
		//update existing template
		if (id) {
			Survey.updateOne({_id:id, owner:res.locals.user.login},survey, function(err) {
				if (err) { debug(err); return res.json([{msg:'database-error'}]);}
				return res.json({});
			})
		}
		//save new template
		else {
			var newSurvey=Survey(survey);
			newSurvey.save(	function(err) {
				if (err) { debug(err); return res.json([{msg:'database-error'}]);}
				return res.json({});
			})
		}
	}
]

// Parse json format
function parseJson(data) {
	var survey={};
	if (!data) return survey;
	//validate meta data
	survey.name=data.name && data.name.match(consts.REGEX_TEXT_300) ? data.name : '',
	survey.desc=data.desc && data.desc.match(consts.REGEX_TEXT_300) ? data.desc : '',
	survey.visible=data.visible;
	//validate parts
	survey.parts=[];
	if (data.parts) {
		for (dp of data.parts) {
			var part={};
			part.title=dp.title && dp.title.match(consts.REGEX_TEXT_100) ? dp.title : '';
			part.questions=[];
			if (dp.questions) {
				for (dq of dp.questions) {
					var question={};
					question.text=dq.text && dq.text.match(consts.REGEX_TEXT_100) ? dq.text : '';
					question.type=dq.type && dq.type.toString().match(/^(1|2|3|4)$/) ? dq.type : 0;
					question.options=dq.options && dq.options.match(consts.REGEX_TEXT_300) && dq.type==4 ? dq.options : '';
					part.questions.push(question);
				}
			}
			survey.parts.push(part);
		}

	}
	return survey;
}


/********************************************************************************
 Participants
 ********************************************************************************/

//load instance from ticket
exports.loadInstanceFromTicket=[
	check('ticket').matches(/^[0-9]{1,6}-[0-9a-zA-Z]{1,100}$/).withMessage('ticket-invalid'),
	(req, res, next)=> {
		if (!validationResult(req).isEmpty()) return res.json([{msg:'validation-error'}]);
		//Parse ticket (post and get)
		var ticket=req.params.ticket || req.body.ticket || req.query.ticket;
		if (!ticket) return res.json([{msg:'validation-error'}]);
		var number=(ticket.split('-'))[0];
		//Load instance
		SurveyInstance.findOne({number: number}).exec(function(err,instance) {
			if (err) debug(err);
			if (!instance) return res.json([{msg:'ticket-invalid'}]);
			if (instance.tickets.indexOf(ticket)<0) return res.json([{msg:'ticket-invalid'}]);
			res.locals.instance=instance;
            res.locals.ticket=ticket;
			next();
		});
	}
]
 //return instance
exports.getPartInstance=function(req,res,next) {
	return res.json(res.locals.instance);
}

//add vote
exports.addVote=function(req,res,next) {
	var instance=res.locals.instance;
	var answers=req.body.answers;
	if (!answers) return res.json([{msg:'error'}])
	var answer=new SurveyAnswer({instance: instance._id, answers: [] });
	for (i=0; i<instance.parts.length;i++) {
		var part=instance.parts[i];
		for (j=0; j<part.questions.length; j++) {
			question=part.questions[j];
			var input=answers[question._id];
			if (!input) input="";
			if (!(input.toString().match(consts.REGEX_TEXT_300))) input="";
			answer.answers.push(input);
		}
	}
	//Save answer
	answer.save(function(err) {
		if (err) { debug(err); return res.json([{error: 'unknown-error'}]);}
		if (instance.invalidateTickets) {
			instance.tickets.splice(instance.tickets.indexOf(res.locals.ticket),1);
		}
		instance.ts=Date.now();
		instance.save(function(err) {
			if (err) { debug(err); return res.json([{error: 'unknown-error'}]);}
			return res.json({});
		})
	})
}



/********************************************************************************
 Admin
 ********************************************************************************/

//Return all surveys for admin
exports.getAdminSurveys=function(req, res, next) {
	Survey.find().sort({ owner:'asc', title: 'asc'}).exec(function(err,allSurveys) {
		if (err) {
			debug(err);
			return res.json([{msg:'database-error'}])
		}
		res.json(allSurveys)
	});
}

//Return all instances for admin
exports.getAdminInstances=function(req, res, next) {
	SurveyInstance.find().sort({ owner:'asc', title: 'asc'}).exec(function(err,allSurveyInstances) {
		if (err) {
			debug(err);
			return res.json([{msg:'database-error'}])
		}
		res.json(allSurveyInstances)
	});
}

/*******************************************************
 Utilities
 *******************************************************/

 /*
 Delete all user data - only used from user controller
 */
 exports.deleteUserData=function(login) {
 	return new Promise(async (resolve)=> {
		//Delete instances
		var instancesToDelete=await SurveyInstance.find({owner:login});
		for (var instance of instancesToDelete) {
			await SurveyAnswer.deleteMany( { instance:instance._id});
			await SurveyInstance.deleteOne( { _id: instance._id});
		}
		//Delete templates
		await Survey.deleteMany({owner:login});
		resolve();
 	})
 }
