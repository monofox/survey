var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var SurveyPartSchema=require('./modelSurveyPartSchema.js');

/*
In the current survey, a complete copy of the collection of questions is kept (parts),
  so that if the questionnaire is changed, the evaluation of existing surveys is still possible..
 */
var SurveyInstanceSchema=new Schema({
	title:				{type: String, required: true, max:300},
	surveyName:			{type: String, max:300},
	number:				{type: Number, required: true, unique:true},
	owner:				{type: String, max: 100},
	tickets:			[{type: String}],
	invalidateTickets:	{type: Boolean},
	parts:				[SurveyPartSchema],
	created:			{type: Date},
	ts:					{type: Number, default: 0}
});


module.exports=mongoose.model('SurveyInstance', SurveyInstanceSchema );
